package com.example.NutriPhi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class ScrollViewTest extends AppCompatActivity {
    private CardView cardView_1;
    private CardView cardView_2;
    private CardView cardView_3;
    private CardView cardView_4;
    private CardView cardView_5;
    private CardView cardView_6;
    private CardView cardView_7;
    private CardView cardView_8;

    FirebaseAuth mFirebaseAuth;
    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scroll_view_test);

        cardView_1 = findViewById(R.id.first_card);
        cardView_2 = findViewById(R.id.second_card);
        cardView_3 = findViewById(R.id.third_card);
        cardView_4 = findViewById(R.id.fourth_card);
        cardView_5 = findViewById(R.id.fifth_card);
        cardView_6 = findViewById(R.id.sixth_card);
        cardView_7 = findViewById(R.id.seventh_card);
        cardView_8 = findViewById(R.id.eighth_card);



        cardView_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_1 = new Intent(ScrollViewTest.this, HomeActivity.class);
                startActivity(intent_1);
            }
        });

        cardView_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_2 = new Intent(ScrollViewTest.this, Services.class);
                startActivity(intent_2);
            }
        });

        cardView_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_3 = new Intent(ScrollViewTest.this, Species.class);
                startActivity(intent_3);
            }
        });
        cardView_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_4 = new Intent(ScrollViewTest.this, Products.class);
                startActivity(intent_4);

            }
        });

        cardView_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_6 = new Intent(ScrollViewTest.this, Feedback.class);
                startActivity(intent_6);
            }
        });

        cardView_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent_7 = new Intent(ScrollViewTest.this, ContactUs.class);
                startActivity(intent_7);
            }
        });



    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.profile)
        {
            Intent i = new Intent(ScrollViewTest.this, Profile.class);
            startActivity(i);
        }

        if(id == R.id.exit)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(ScrollViewTest.this);
            builder.setMessage("Do You Want To Exit?");
            builder.setCancelable(true);

            builder.setNegativeButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finishAffinity();
                }
            });

            builder.setPositiveButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }
        return true;
    }



}
